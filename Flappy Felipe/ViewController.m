//
//  ViewController.m
//  Flappy Felipe
//
//  Created by Main Account on 2/13/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@interface ViewController () <MySceneDelegate>
@end

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  // Configure the view.
  SKView * skView = (SKView *)self.view;
  skView.showsFPS = YES;
  skView.showsNodeCount = YES;
  
  // Create and configure the scene.
  SKScene * scene = [[MyScene alloc] initWithSize:skView.bounds.size delegate:self];
  scene.scaleMode = SKSceneScaleModeAspectFill;
  
  // Present the scene.
  [skView presentScene:scene];
    
    [Kamcord setDeveloperKey:@"d3y5JcHlXzoJHtJW2jNA27BO8jvOcBAKUNjdOlhZX1R"
         developerSecret:@"eZUPOPnBZGT314BYsdj4uRs6UAhDmki7XgSnsyy2H2G"
                 appName:@"Flappy Boo"
    parentViewController:self];
}

- (BOOL)shouldAutorotate
{
  return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return UIInterfaceOrientationMaskAllButUpsideDown;
  } else {
    return UIInterfaceOrientationMaskAll;
  }
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
  return YES;
}

- (UIImage *)screenshot {
  UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 1.0);
  [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

- (void)shareString:(NSString *)string url:(NSURL*)url image:(UIImage *)image {
  UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:@[string, url, image] applicationActivities:nil];
  [self presentViewController:vc animated:YES completion:nil];

}

@end
